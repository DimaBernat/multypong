﻿using UnityEngine;

public class Player : MonoBehaviour
{
    /// <summary>
    /// Player identifier enum
    /// </summary>
    public enum PlayerType
    {
        Left,
        Right
    }

    #region Editor exposed members
    [SerializeField] private PlayerType _playerType;
    [SerializeField] private float _movementSpeed = 5;
    #endregion

    #region Private members
    private Transform _transform;
    private float _halfHeight;
    private Vector3 m_inputPlayer;
    private Rigidbody m_rigiBody;
    #endregion

    private void Start()
    {
        // Store highly used variables in advance for performance
        _transform = transform;
        _halfHeight = GetComponent<Collider>().bounds.extents.y;
       
        m_rigiBody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // TODO: Get movement input (Make sure left/right player)
        // TODO: Move player
        if (_playerType == 0)
        {           
            m_inputPlayer.y = Input.GetAxis("Player2") * Time.deltaTime * _movementSpeed;
            m_rigiBody.velocity += new Vector3(0, m_inputPlayer.y, 0);
        }
        else
        {           
            m_inputPlayer.y = Input.GetAxis("Player1") * Time.deltaTime * _movementSpeed;
            m_rigiBody.velocity += new Vector3(0, m_inputPlayer.y, 0);
        }


        // TODO: Make sure player doesn't leave screen bounds (ScreenUtil.ScreenPhysicalBounds will help you out)       

        //Pysics does half the job as RigidBody hits the walls. even if I didnt used player movment with rigidbody, But as I fully move with pysics it will be bound to screen as the walls right on top of the screen bound.(Dima)
    }
}