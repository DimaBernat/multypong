﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class Ball : MonoBehaviour
{
    #region Editor exposed members
    [SerializeField] private float _minVelocity = 5;
    #endregion

    #region Events
    public event Action<EndZone.EndZoneType> EnteredEndZone;
    #endregion

#region private
    private Rigidbody m_rigiBody;
#endregion

    private void Start()
    {
            m_rigiBody = GetComponent<Rigidbody>();
       
  
    }
    /// <summary>
    /// Gives the ball a completely random velocity (50%/50% left/right + 50%/50% up/down) with the minimum velocity
    /// </summary>
    public void GiveRandomVelocity()
    {
        // TODO: Give our rigidbody a random velocity, 50%/50% left/right + 50%/50% up/down - must be at least at _minVelocity
        m_rigiBody.velocity = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
        m_rigiBody.velocity = m_rigiBody.velocity * _minVelocity;
        if (m_rigiBody.velocity.magnitude < _minVelocity)
        {
            float ratio = _minVelocity / m_rigiBody.velocity.magnitude;
            m_rigiBody.velocity = m_rigiBody.velocity * ratio;          
        }
    }

    /// <summary>
    /// Resets the ball (position and velocity)
    /// </summary>
    public void Reset()
    {
        // TODO: Reset our ball's position and velocity
        m_rigiBody.position =  Vector3.zero;
        
       
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Make sure if the ball lost velocity that we're never below the minimum
        if (m_rigiBody.velocity.magnitude < _minVelocity)
        {
            float ratio = _minVelocity / m_rigiBody.velocity.magnitude;

            m_rigiBody.velocity = m_rigiBody.velocity * ratio;
        }
        //makeing sure ball will never be in a full Vartical movment
        if (m_rigiBody.velocity.y > 0.75 || m_rigiBody.velocity.y < -0.75)
        {
            //m_rigiBody.velocity.y = m_rigiBody.velocity.y * 0.9f;
            
            m_rigiBody.velocity = new Vector3(m_rigiBody.velocity.x, m_rigiBody.velocity.y * 0.8f, 0);
            
        }
        //m_rigiBody.velocity = new Vector3(m_rigiBody.velocity.x * 1.1f, m_rigiBody.velocity.y * 0.9f, 0);
        
    }

    private void OnTriggerEnter(Collider collider)
    {
        // TODO: Handle trigger collisions for endzones
        // TODO: Make sure we collided with an endzone
        // TODO: Raise the EnteredEndZone event if we did
        if (collider.GetComponent<EndZone>())
        {           
            EnteredEndZone.Invoke(collider.GetComponent<EndZone>().EndZoneSide);     
        }
        m_rigiBody.velocity = Vector3.zero;
        m_rigiBody.position = Vector3.zero;
    }
}