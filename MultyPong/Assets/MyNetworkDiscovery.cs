﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MyNetworkDiscovery : NetworkDiscovery
{

    public bool IsServer ;
    // Use this for initialization
    void Start()
    {
        Initialize();
        StartAsServer();
        if (IsServer)
        {   
            
        }
        /*else
        {
            Initialize();
            StartAsClient();
        }*/
      
    }


    // Update is called once per frame
    void Update()
    {

    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        if (!IsServer)
        {
            print("DA");
            Debug.Log("Adress: " + fromAddress + " Data: " + data);
            NetworkManager.singleton.networkAddress = fromAddress;
            NetworkManager.singleton.StartClient();
        }
        
    }
}
