﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class NetClient : NetworkDiscovery
{   
    string _netIP, _netPort;

    void Start()
    {
        startClient();
        showGUI = false;
    }

    

    private void OnDestroy()
    {
        //Dima - Disabled as it will taken care in LobbyManger
        //GetComponent<MyNetworkManager>().StartHost();
    }
    public void startClient()
    {
        Initialize();
        StartAsClient();
        Debug.Log("Searching for Server");
        useNetworkManager = true;
    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        Debug.Log("Adress:" + fromAddress + " Port: " + data);

        
        //Split the IP from the string
        string[] stringy = fromAddress.Split(':');
        foreach (string ip in stringy)
        {

            /*if (ip.Contains("1") || ip.Contains("0"))
            {
                
                GetComponent<MyNetworkManager>().networkAddress = ip;
                _netIP = ip;
                if (GetComponent<MyNetworkManager>().networkPort == int.Parse(_netPort) && GetComponent<MyNetworkManager>().networkAddress == _netIP)
                {
                    GetComponent<MyNetworkManager>().StartClient();
                }
            }*/

            if (ip.Contains("1") || ip.Contains("0"))
            {
                
                
                Debug.Log("Server Found " + ip);
                
                Destroy(this);
            }
            else
            {
                Debug.Log("Didn't find server ");
                Destroy(this);
            }
            
            //LobbyMainMenu
        }

        //Changeing to Send Ip + port Via string

        /*if (GetComponent<MyNetworkManager>() != null)
        {

            SplitString(data);

        }*/

        
    }

    void SplitString(string data) {
        string[] stringy = data.Split(':');
        foreach (string port in stringy)
        {
            if (port.Contains("1") || port.Contains("0") || port.Contains("2") || port.Contains("3") || port.Contains("4") || port.Contains("5") || port.Contains("6") || port.Contains("7") || port.Contains("8") || port.Contains("9"))
            {
                
                GetComponent<MyNetworkManager>().networkPort = int.Parse(port);
                _netPort = port;
                if (GetComponent<MyNetworkManager>().networkPort == int.Parse(_netPort) && GetComponent<MyNetworkManager>().networkAddress == _netIP)
                {
                    //GetComponent<MyNetworkManager>().StartClient();
                }
               
            }
        }
    }
}