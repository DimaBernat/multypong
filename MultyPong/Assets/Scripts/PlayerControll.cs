﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControll : MonoBehaviour {
    public float m_speed;

    private Vector3 inputPlayer;
    private float f_Top, f_Bottem,f_fractsion = 0.01f;
	private Rigidbody _rigi;

    void Start()
    {
        //Calculating Hegiht of screen for player movement Restricstion;
       var v_Bottem = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        f_Bottem = v_Bottem.y/2;
        f_Top = -1*(v_Bottem.y / 2);
		_rigi = GetComponent<Rigidbody> ();
    }
	// Update is called once per frame
	void Update () {
        //Geting Input of player andtranslating it to movment;
		inputPlayer.y = Input.GetAxis("Player2") * Time.deltaTime * m_speed;
        //Moveing our player
		_rigi.velocity += new Vector3(0,inputPlayer.y,0);

        //Clamping movemant to screen for later
       
    }
}
