﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput.PlatformSpecific;


public class PlayerControllMulti : NetworkBehaviour {
	[SyncVar]
	public float test;
	public float f_speed;
	public GameObject G_Ball;
	private Vector3 inputPlayer;

	//private BallManger SCR_ballManger;
	private Rigidbody _rigi;

	// Use this for initialization
	void Start () {
		//Destroy if not local
		if (!isLocalPlayer)
		{
			Destroy (this);
		}
		_rigi = GetComponent<Rigidbody> ();
        //moveing players ito the right spot.
        if (isServer)
        {

            _rigi.position = new Vector3(7, 0, 0);
            //transform.position = (new Vector3 (7, 0, 0));
            Invoke("FreezeRigidBody",1 );
        }
        else
        {
            _rigi.position = new Vector3(-7, 0, 0);
            _rigi.drag = 0f;
            //_rigi.velocity = new Vector3(0, 50, 0); //Dima Test movemant
            //transform.position = (new Vector3 (-7, 0, 0));
            Invoke("FreezeRigidBody", 1);
        }

        //Check amount of players in game And then start game
        if (GameObject.FindGameObjectsWithTag("Player").Length == 2)
        {
            //NetworkServer.Spawn (G_Ball); //CmdSpawnBall ();
            CmdSpawnBall();

        }

        

    }


	// Update is called once per frame
	void FixedUpdate () {
       
		//Geting Input of player andtranslating it to movment;
		inputPlayer.y = Input.GetAxis("Player2") * Time.deltaTime * f_speed;
		//Moveing our player
		//transform.Translate(inputPlayer);
		//Clamping movemant to screen for later
		_rigi.velocity += new Vector3(0,inputPlayer.y,0); //(_rigi.position + (new Vector3(0,inputPlayer.y,0))* Time.deltaTime);

		//For Test
		if (!isServer) {
			//_rigi.velocity = new Vector3 (0,5,0);
		}
    }

    void FreezeRigidBody() {
        _rigi.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ; 
    }

	[Command]
	 void CmdSpawnBall(){
		GameObject instance = Instantiate (G_Ball)as GameObject;
		NetworkServer.Spawn (instance);
	}
}
