﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
//Network
using System.Net;
using System.Net.Sockets;

public class BallMangerMulty : NetworkBehaviour {
	//public Text txt_left,txt_right; 
	public float f_EndGame;
	public bool isMultiyPlayer;

	private float f_left, f_right;
	bool isGameEnd,areGameOn;
	Rigidbody rigi;
	// Use this for initialization
	void Start () {
		if (!isServer)
		{
			Destroy (this);
		}
		restartScore ();
		rigi = GetComponent<Rigidbody>();
		if (!isMultiyPlayer) {
			CmdBallLocaston (); 	
		}
	}

	// Update is called once per frame
	//[ServerCallback]
	void FixedUpdate () {
		
		if (Input.GetKeyDown(KeyCode.Space)) {
			transform.position =new Vector3(0,0,0);
			rigi.velocity = Vector3.zero;
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (isServer) {
			rigi.velocity = new Vector3 (rigi.velocity.x * 1.1f, rigi.velocity.y * 0.9f, 0);
			if (rigi.velocity.y == 0) {
				rigi.velocity = new Vector3 (rigi.velocity.x, Random.Range (-2, 2), 0);
				print (Random.Range (-1, 1));
			}
		}
	}

	void OnTriggerEnter(Collider coll)
	{		
		if (isServer) {
			CmdBallLocaston ();
		}	
	}

	void restartScore(){
		f_right = 0;
		f_left = 0;
		//txt_left.text = f_left.ToString();
		//txt_right.text = f_right.ToString();
	}


	[Command]
	 void CmdBallLocaston(){		
		Debug.Log ("restart");
		rigi.position = new Vector3(0,0,0);
		rigi.velocity = Vector3.zero;
		rigi.AddForce(new Vector3(500, Random.Range(0,50), 0));
	}
}
