﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//Network
using System.Net;
using System.Net.Sockets;

public class BallManger : MonoBehaviour {
	public Text txt_left,txt_right; 
	public float f_EndGame;
	public bool isMultiyPlayer;

	private float f_left, f_right;
	bool isGameEnd,areGameOn;
    Rigidbody rigi;
	// Use this for initialization
	void Start () {
		restartScore ();
        rigi = GetComponent<Rigidbody>();
		if (!isMultiyPlayer) {
			BallLocaston ();			
		}
    }
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Space)) {
			transform.position =new Vector3(0,0,0);
			rigi.velocity = Vector3.zero;
		}
    }

	//on collision Add force a little
	void OnCollisionEnter(Collision collision)
	{
		//Check Velocity of Y and change movements depaneds on your projactile
		if (rigi.velocity.y >= 1 || rigi.velocity.y <= -1) {
			rigi.velocity = new Vector3 (rigi.velocity.x * 1.05f, rigi.velocity.y * 0.95f, 0);
		} else {
			float test = Random.Range (-2.0f, 2.0f);
			rigi.velocity = new Vector3 (rigi.velocity.x,test,0);
			print (test);
		}
	}

	//Take care when you out side of game zone
    void OnTriggerEnter(Collider coll)
    {		
		
        //Basic LoseWin detacstion for now just restarting For Latter trigger effact lose or win depands on player
        if (coll.CompareTag("Finish"))
        {
            if (coll.name == "Left")
            {
				f_right++;
				txt_right.text = f_right.ToString();
				BallLocaston ();
				/*if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name
					==  "Practice") 
				{					
					UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
				}  */
				if (f_right == f_EndGame) {
					isGameEnd = true;
				}
            }
            if (coll.name == "Right")
            {
				f_left++;
				txt_left.text = f_left.ToString();
				BallLocaston ();
				if (f_left == f_EndGame) {
					isGameEnd = true;
				}
            }
        }        
    }

	void restartScore(){
		f_right = 0;
		f_left = 0;
		txt_left.text = f_left.ToString();
		txt_right.text = f_right.ToString();
	}


	//Start 
	public void BallLocaston(){		
		Debug.Log ("restart");
		transform.position =new Vector3(0,0,0);
		rigi.velocity = Vector3.zero;
		rigi.AddForce(new Vector3(500, Random.Range(0,50), 0));
	}
}
