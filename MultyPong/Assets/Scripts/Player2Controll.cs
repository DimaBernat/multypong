﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controll : MonoBehaviour {
    public float f_speed;

    private Vector3 inputPlayer;
    private float f_Top, f_Bottem,f_fractsion = 0.01f;

    void Start()
    {
        //Calculating Hegiht of screen for player movement Restricstion;
       var v_Bottem = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        f_Bottem = v_Bottem.y/2;
        f_Top = -1*(v_Bottem.y / 2);
        
    }
	// Update is called once per frame
	void Update () {
        //Geting Input of player andtranslating it to movment;
		inputPlayer.y = Input.GetAxis("Player2") * Time.deltaTime * f_speed;

        //Moveing our player
        transform.Translate(inputPlayer);

        //Clamping movemant to screen for later
       
    }
}
